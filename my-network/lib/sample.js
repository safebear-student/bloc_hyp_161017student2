
/** 
* @param {org.acme.mynetwork.Trade} trade - the trade to be processed
* @transaction  
*/

function tradeCommodity(trade) {

    trade.commodity.owner = trade.newOwner;
    
    // Get the asset registry for the asset.
    return getAssetRegistry('org.acme.mynetwork.Commodity')
        .then(function (assetRegistry) {
            // Update the asset in the asset registry.
            return assetRegistry.update(trade.commodity);
        });
}
